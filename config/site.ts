export const siteConfig = {
  name: 'Kyle\'s Blog',
  url: 'https://kyles-blog-24.vercel.app',
  description: 'Nextjs 14 blog utilizing velite, shadcn, and tailwindcss',
  author: 'Kyle Lynch',
  links: {
    gitlab: 'https://gitlab.com/kyleknollelynch',
    github: 'https://github.com/kyleknollelynch',
    personalWebsite: 'https://kylelynch.me',
  },
}

export type SiteConfig = typeof siteConfig
