'use client'

import { zodResolver } from '@hookform/resolvers/zod'
import { useForm } from 'react-hook-form'
import { z } from 'zod'
import axios from 'axios'
import { useRouter } from 'next/navigation'
import { Send } from 'lucide-react'
import { useToast } from '@/components/ui/use-toast'

import { Button, buttonVariants } from '@/components/ui/button'
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form'
import { Input } from '@/components/ui/input'
import { Textarea } from '@/components/ui/textarea'
import { cn } from '@/lib/utils'

const formSchema = z.object({
  botField: z.string().optional(),
  name: z
    .string()
    .min(2, {
      message: 'Name must be at least 2 characters.',
    })
    .max(50, {
      message: 'Name is 50 characters maximum.',
    }),
  email: z.string().email({ message: 'Please enter a valid email address.' }),
  message: z
    .string()
    .min(2, {
      message: 'Message must be at least 2 characters',
    })
    .max(200, {
      message: 'Message is 200 characters maximum.',
    }),
})

export default function ContactForm() {
  const router = useRouter()
  const { toast } = useToast()
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      botField: '',
      name: '',
      email: '',
      message: '',
    },
  })

  function onSubmit(data: z.infer<typeof formSchema>) {
    console.log(data)

    if (data.botField !== '') return

    axios
      .post(`${process.env.NEXT_PUBLIC_PIPEDREAM_WEBHOOK_URL}`, data)
      .then(response => {
        router.push('/')
        toast({
          title: 'Submission successfull!',
          description: 'I will get back to you shortly.',
        })
      })
      .catch(e =>
        toast({
          title: 'Uh oh!',
          description: 'Something went wrong.',
        }),
      )
  }

  return (
    <Form {...form}>
      <form
        onSubmit={form.handleSubmit(onSubmit)}
        className='space-y-8'
      >
        <FormField
          control={form.control}
          name='botField'
          render={({ field }) => (
            <FormItem className='hidden'>
              <FormLabel>
                Don&apos;t fill this out if you&apos;re human:
              </FormLabel>
              <FormControl>
                <Input {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name='name'
          render={({ field }) => (
            <FormItem>
              <FormLabel className='text-lg font-bold'>Name</FormLabel>
              <FormControl>
                <Input placeholder='Your name' {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name='email'
          render={({ field }) => (
            <FormItem>
              <FormLabel className='text-lg font-bold'>Email</FormLabel>
              <FormControl>
                <Input placeholder='Your email' type='email' {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name='message'
          render={({ field }) => (
            <FormItem>
              <FormLabel className='text-lg font-bold'>Message</FormLabel>
              <FormControl>
                <Textarea placeholder='Your message' {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <Button
          type='submit'
          className={cn(buttonVariants({ size: 'lg' }), 'w-full sm:w-fit')}
        >
          Submit
          <Send className='ml-2 h-4 w-4' />
        </Button>
      </form>
    </Form>
  )
}
