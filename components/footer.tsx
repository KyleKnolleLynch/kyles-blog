import { siteConfig } from '@/config/site'

export default function Footer() {
  return (
    <footer>
      <div className='mb-6 mt-14 flex flex-col items-center'>
        <div className='mb-3 flex space-x-4 text-sm text-muted-foreground'>
          <a href={siteConfig.links.personalWebsite} target='_blank'>
            {siteConfig.author}
          </a>
        </div>
      </div>
    </footer>
  )
}
