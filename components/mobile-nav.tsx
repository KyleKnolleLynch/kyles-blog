'use client'

import { useState } from 'react'
import Link, { LinkProps } from 'next/link'
import { Menu } from 'lucide-react'
import { Sheet, SheetContent, SheetTrigger } from '@/components/ui/sheet'
import { Button } from './ui/button'
import { useRouter } from 'next/navigation'
import { Icons } from './icons'
import { siteConfig } from '@/config/site'

export default function MobileNav() {
  const [open, setOpen] = useState(false)
  return (
    <Sheet open={open} onOpenChange={setOpen}>
      <SheetTrigger asChild>
        <Button variant='outline' className='w-10 px-0 sm:hidden'>
          <Menu className='h-5 w-5' />
          <span className='sr-only'>Toggle Nav Menu</span>
        </Button>
      </SheetTrigger>
      <SheetContent side='right'>
        <MobileLink
          href='/'
          onOpenChange={setOpen}
          className='flex items-center'
        >
          {/* <Icons.logo className='mr-2 h-4 w-4' /> */}
          <span className='font-bold'>{siteConfig.name}</span>
        </MobileLink>
        <div className='mt-3 flex flex-col gap-3'>
          <MobileLink href='/blog' onOpenChange={setOpen}>
            Blog
          </MobileLink>
          <MobileLink href='/about' onOpenChange={setOpen}>
            About
          </MobileLink>
          <MobileLink href='/contact' onOpenChange={setOpen}>
            Contact
          </MobileLink>
          <Link href={siteConfig.links.gitlab} target='_blank' rel='noreferrer'>
            GitLab
          </Link>
          <Link href={siteConfig.links.github} target='_blank' rel='noreferrer'>
            GitHub
          </Link>
        </div>
      </SheetContent>
    </Sheet>
  )
}

interface MobileLinkProps extends LinkProps {
  children: React.ReactNode
  onOpenChange?: (open: boolean) => void
  className?: string
}

function MobileLink({
  children,
  onOpenChange,
  href,
  className,
  ...props
}: MobileLinkProps) {
  const router = useRouter()

  return (
    <Link
      href={href}
      onClick={() => {
        router.push(href.toString())
        onOpenChange?.(false)
      }}
      className={className}
      {...props}
    >
      {children}
    </Link>
  )
}
