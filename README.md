# Kyle's Blog

🔗 [live site](https://kyles-blog-24.vercel.app)

My new blog built with Nextjs 14, Velite, ShadCN/UI, and TailwindCSS.

Credit and big thanks to [Jolly Coding](https://www.youtube.com/@JollyCoding) for inspiration and code base for helping me to build this blog.