import { Avatar, AvatarFallback, AvatarImage } from '@/components/ui/avatar'
import { siteConfig } from '@/config/site'
import { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'About Me',
  description: 'Information about me'
}

export default async function About() {
  return (
    <div className='container max-w-6xl py-6 lg:py-10'>
      <section className='flex flex-col items-start gap-4 md:flex-row md:justify-between'>
        <div className='flex-1 space-x-4'>
          <h1 className='inline-block text-4xl font-black lg:text-5xl'>
            About Me
          </h1>
        </div>
      </section>
      <hr className='my-8' />
      <div className='flex flex-col items-center gap-8 md:flex-row md:items-start'>
        <div className='flex min-w-48 max-w-48 flex-col gap-2'>
          <Avatar className='h-48 w-48'>
            <AvatarImage
              src='/avatar.jpg'
              alt={siteConfig.author}
              className='object-cover'
            />
            <AvatarFallback>KL</AvatarFallback>
          </Avatar>
          <h2 className='font-2xl break-words text-center font-bold'>
            Kyle Lynch
          </h2>
          <p className='break-words text-center text-muted-foreground'>
            Web Developer
          </p>
        </div>
        <p className='py-4  text-lg text-muted-foreground'>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum
          corporis excepturi eveniet sint expedita doloribus ipsam
          necessitatibus blanditiis? Eligendi rerum nam distinctio illum
          sapiente, dolorum quibusdam expedita aspernatur minus nihil.
        </p>
      </div>
    </div>
  )
}
