import ContactForm from '@/components/contact-form'
import { Card, CardContent } from '@/components/ui/card'

export default function Contact() {
  return (
    <div className='container max-w-4xl py-6 lg:py-10'>
      <section className='flex flex-col items-start gap-4 md:flex-row md:justify-between md:gap-8'>
        <div className='flex-1 space-y-4'>
          <h1 className='inline-block text-4xl font-black lg:text-5xl'>
            Contact Me
          </h1>
        </div>
      </section>
      <hr className='my-8' />
      <Card className='sm:p-3'>
        <CardContent className='px-4 sm:px-6'>
          <ContactForm />
        </CardContent>
      </Card>
    </div>
  )
}
