import { Metadata } from 'next'
import { getAllTags, sortTagsByCount } from '@/lib/utils'
import { posts } from '#site/content'
import { Tag } from '@/components/tag'

export const metadata: Metadata = {
  title: 'Tags',
  description: 'Topics of articles in my blog',
}

export default async function TagsPage() {
  const tags = getAllTags(posts)
  const sortedTags = sortTagsByCount(tags)

  return (
    <div className='container max-w-4xl py-6 lg:py-10'>
      <section className='flex flex-col items-start gap-4 md:flex-row md:justify-between md:gap-8'>
        <div className='flex-1 space-y-4'>
          <h1 className='inline-block text-4xl font-black lg:text-5xl'>Tags</h1>
        </div>
      </section>
      <hr className='my-4' />
      <section className='flex flex-wrap gap-2'>
        {sortedTags?.map(tag => <Tag key={tag} tag={tag} count={tags[tag]} />)}
      </section>
    </div>
  )
}
